# Start with the base, container-managed image from Camunda.
FROM camunda/camunda-bpm-platform:7.11.0

USER camunda:camunda

# Remove the default "camunda-invoice" directory from /camunda/webapps,
#   thereby eliminating the sample process definitions.
RUN rm -rf /camunda/webapps/camunda-invoice

# Copy the sample generated WAR file to the /camunda/webapps directory.
COPY --chown=camunda:camunda target/*.war /camunda/webapps

# Copy the "bpm-platform.xml" file, overwriting the stock file. This 
#   mechanism could be used for any additional changes that need 
#   to be made to the configuration (in that file), such as 
#   the specification of information for an LDAP connection.
COPY --chown=camunda:camunda docker/conf/bpm-platform.xml /camunda/conf

# Copy the "engine-security.xml" file, overwriting the web.xml file. This 
#   configuration enables Basic Authentication for camunda engine. So we are
#   able to use authentication functions inside of java delegates.
COPY --chown=camunda:camunda docker/conf/engine-security.xml /camunda/webapps/engine-rest/WEB-INF/web.xml

# Copy initial predefined camunda database that contains user model
#   user / user - All Users, Users
#   manager / manager - All Users, Managers
#   admin / admin - All Users, camunda administrator
COPY --chown=camunda:camunda docker/db/process-engine.mv.db /camunda/camunda-h2-dbs/process-engine.mv.db

# Copy Oracle jdbc JAR file.
COPY --chown=camunda:camunda docker/lib/* /camunda/lib
