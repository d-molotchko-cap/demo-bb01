package com.capbpm.application;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import org.joda.time.DateTime;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.module.SimpleModule;

//@Configuration
public class JacksonDateTimeConfiguration {

	private final static String[] formats = {
	"yyyy-MM-dd'T'HH:mm:ss'Z'",   "yyyy-MM-dd'T'HH:mm:ssZ",
    "yyyy-MM-dd'T'HH:mm:ss",      "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
    "yyyy-MM-dd'T'HH:mm:ss.SSSZ", "yyyy-MM-dd HH:mm:ss",
    
    "MM/dd/yyyy", 				  "MM/dd/yyyy HH:mm", 
    "MM/dd/yyyy HH:mm:ss",        "MM/dd/yyyy'T'HH:mm:ss.SSS'Z'", 
    "MM/dd/yyyy'T'HH:mm:ss.SSSZ", "MM/dd/yyyy'T'HH:mm:ss.SSS", 
    "MM/dd/yyyy'T'HH:mm:ssZ",     "MM/dd/yyyy'T'HH:mm:ss",
    
    "yyyy:MM:dd HH:mm:ss",        "yyyyMMdd"
    };
	
	private static class CustomerDateAndTimeDeserialize extends JsonDeserializer<Date> {
		
		@Override
		public Date deserialize(JsonParser paramJsonParser, DeserializationContext paramDeserializationContext) {
			final String[] str = {null};
			try {
				str[0] = paramJsonParser.getText().trim();
				final DateTime retval = new DateTime(str[0]);
				return retval.toDate();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				final Date date = Arrays.asList(formats)
					.stream()
					.map(f->{
						return parseDate(f, str[0]);
					})
					.filter(d->d != null)
					.findAny()
					.orElse(null);
				if( date != null ) {
					return date;
				}
			}
			return new Date();
		}

		private Date parseDate(String format, String str) {
			if( str != null ) {
				try {
					final SimpleDateFormat sdf = new SimpleDateFormat(format);
					final Date date = sdf.parse(str);
					return date;
				} catch (ParseException e) {
				} catch (IllegalArgumentException e) {
				}
			}
			return null;
		}
	}	
	
    @Bean
    public Module module() {
        SimpleModule module = new SimpleModule("DateTime module", new Version(1, 0, 0, null, null, null));
        module.addDeserializer(Date.class, new CustomerDateAndTimeDeserialize());
        return module;
    }
}
