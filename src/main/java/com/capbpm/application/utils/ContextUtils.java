package com.capbpm.application.utils;

import javax.servlet.http.HttpServletRequest;

public class ContextUtils {

    public static String getBaseUrl(HttpServletRequest request) {
        return request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
    }
}
