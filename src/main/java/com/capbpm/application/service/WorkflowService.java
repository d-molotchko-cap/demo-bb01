package com.capbpm.application.service;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

import com.capbpm.application.model.BoTracking;
import com.capbpm.application.model.ProcessInstance;
import com.capbpm.application.model.Statistic;

public interface WorkflowService {
	String start(String token, String acronym, String body);
	List<ProcessInstance> getInstances(String token, String[] acronyms);
	List<ProcessInstance> getInstances(String token, String[] acronyms, String sql, Map<String, String> mappings);
	String getTaskData(String token, String tkiid);
	String finishTask(String token, BoTracking taskParams, String params) throws UnsupportedEncodingException;
	String saveTask(String token, BoTracking taskParams, String params) throws UnsupportedEncodingException;
	String claimTask(String token, BoTracking taskParams) throws UnsupportedEncodingException;

	/**
	 * Provides bpmn 2.0 xml representation of process
	 *
	 * @param key process key (definition id)
	 * @return xml representation of workflow definition
	 *
	 */
	String getXml(String token, String key);

	/**
	 * Provides statistic about runned instances within process
	 *
	 * @param key process key (definition id)
	 * @return statistics
	 */
	List<Statistic> getStatistics(String token, String key);
}
