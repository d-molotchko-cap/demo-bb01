package com.capbpm.application.service;

import java.io.IOException;
import java.util.Properties;

import org.json.JSONObject;

public class Profile {
	
	// TODO: extract active profile from OS environment
	private static String active = "";

	public static void addProperties(JSONObject variables) {
		addProperties(variables, active);
	}

	private static void addProperties(JSONObject variables, String profile) {
		if( !profile.isEmpty() ) {
			loadProfile(variables, "");
		}
		loadProfile(variables, profile);
	}

	private static void loadProfile(JSONObject variables, String profile) {
		if( !profile.isEmpty() ) {
			profile = "-"+profile.toLowerCase();
		}
		try {
			Properties props = new Properties();
			props.load(Profile.class.getResourceAsStream("/env"+profile+".properties"));
			for( Object key: props.keySet()) {
				variables.put("env:"+key, props.getProperty((String) key));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
