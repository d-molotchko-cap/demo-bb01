package com.capbpm.application.service.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import com.capbpm.application.model.BoTracking;
import com.capbpm.application.model.ProcessInstance;
import com.capbpm.application.model.ProcessInstance.AtRisk;
import com.capbpm.application.model.ProcessInstance.Instance;
import com.capbpm.application.model.ProcessInstance.Priority;
import com.capbpm.application.model.ProcessInstance.Subject;
import com.capbpm.application.model.Statistic;
import com.capbpm.application.service.IdentityService;
import com.capbpm.application.service.WorkflowService;
import com.capbpm.application.utils.JSONUtils;

@Service
public class WorkflowServiceImpl extends RestService implements WorkflowService {
	static final private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

    @Value("${proxy.url}")
    private String baseUrl;

    @Value("${application.name}")
    private String applicationName;

    @Autowired
    private IdentityService identityService;
    
    @Autowired
    private JDBCExecutor jdbcExecutor;
    
    @Autowired
    private DataValidationService dataValidationService;
    
	@Override
	public String start(String token, String definitionId, String body) {
		dataValidationService.validate(body);

		final String url = "/process-definition/key/"+definitionId+"/start";
		HttpHeaders headers = new HttpHeaders();
		headers.add(HttpHeaders.CONTENT_TYPE, "application/json");
		JSONObject variables = new JSONObject();
		if( body != null  &&  !body.isEmpty() ) {
			variables.put("variables", JSONUtils.convertToCamunda(new JSONObject(body)));
		}
		final String rc = postREST(identityService.getIdentity(token), url, variables.toString(), headers);
		return rc;
	}

	@Override
	public List<ProcessInstance> getInstances(String token, String[] definitionIds) {
		return getInstances(token, definitionIds, null, null);
	}	

	@Override
	public List<ProcessInstance> getInstances(String token, String[] definitionIds, String sql, Map<String, String> mappings) {
		List<ProcessInstance> rc = new ArrayList<>();
		for( String definitionId : definitionIds ) {
			JSONObject definition = getDefinition(token, definitionId);
			getProcessInstances(token, definitionId).forEach(instance->{
				Map<String, Object>[] data = new Map[] {null};
				
				if( sql != null  &&  sql.contains("{piid}")  &&  !sql.contains("{tkiid}")) {
					data[0] = loadJDBCData(sql.replace("{piid}", instance.getString("id")), mappings );
				}
				
				getTasks(token, instance.getString("id")).forEach(task->{
                    final Integer priority = task.optInt("priority", 50);
                   	Date createdDate;
					try {
						createdDate = sdf.parse(task.optString("created"));
					} catch (ParseException e) {
						createdDate = new Date();
					}
                    final Date atRiskDate = new Date( createdDate.getTime() + 1000*60*60*24*5 );
                    ProcessInstance pi =
						new ProcessInstance(
							new Subject(
								task.getString("id"),
								task.getString("name"),
								task.getString("name"),
								null,
								task.getString("taskDefinitionKey")
							),
							new Instance(
								instance.getString("id"),
								definition.getString("name")+" "+definition.optString("versionTag"),
        						definition.getString("key")
							),
	                        task.optString("assignee", "Not assigned"),
	                        task.optString("created"),
	                        task.optString("due", sdf.format(new Date( createdDate.getTime() + 1000*60*60*24*7 ))),
	                        new AtRisk(
	                    		sdf.format(atRiskDate),
	                    		new Date().after(atRiskDate)
	                        ),
	                        new Priority(
	                        	priority,
	                        	priority == 50 ? "Normal" : (priority < 50 ? "High":"Low")
	                        )
						);
						
                    if( sql != null  &&  sql.contains("{tkiid}") ) {
    					data[0] = loadJDBCData(sql.replace("{piid}", instance.getString("id")).replace("{tkiid}", task.getString("id")), mappings );
                    }
                    if( data[0] != null ) {
                    	pi.getProperties().putAll(data[0]);
                    }
					rc.add(pi);
				});
			});
		}
		return rc;
	}

	private Map<String, Object> loadJDBCData(String sql, Map<String, String> mappings ) {
		final Map<String, Object> data = jdbcExecutor.execute(sql);
		final Map<String, Object> rc = new HashMap<>();

		for( Map.Entry<String, String> entry: mappings.entrySet() ) {
			if( data.containsKey(entry.getKey()) ) {
				rc.put( entry.getValue(), data.get(entry.getKey()) );
			}
		}
		return rc;
	}

	private JSONObject getDefinition(String token, String definitionId) {
		final String url = "/process-definition/key/"+definitionId;
        final JSONObject resp = getRESTObject( identityService.getIdentity(token), url );
		return resp;
	}

	private List<JSONObject> getTasks(String token, String instanceId) {
		final String url = "/task?processInstanceId="+instanceId;
        List<JSONObject> rc = new ArrayList<>();
		
		try {
			final String[] taskVariants = {
				"&unassigned=true&candidateGroupsExpression="+URLEncoder.encode("${currentUserGroups()}", "UTF-8"),
				"&unassigned=true&candidateUserExpression="+URLEncoder.encode("${currentUser()}", "UTF-8"),
				"&assigneeExpression="+URLEncoder.encode("${currentUser()}", "UTF-8"),
			};
			
			Map<String, JSONObject> tasks = new HashMap<>();
			Arrays.asList(taskVariants).forEach(variant->{
		        final JSONArray resp = getRESTArray( identityService.getIdentity(token), url+variant );
		        resp.forEach(obj-> tasks.put(((JSONObject) obj).getString("id"), (JSONObject)obj));
			});
			rc.addAll(tasks.values());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return rc;
	}
	
	private List<JSONObject> getProcessInstances(String token, String definitionId) {
		final String url = "/process-instance?processDefinitionKey="+definitionId;
        final JSONArray resp = getRESTArray( identityService.getIdentity(token), url );
        List<JSONObject> rc = new ArrayList<>();
        resp.forEach(obj->rc.add((JSONObject) obj));
		return rc;
	}

	@Override
	public String getTaskData(String token, String tkiid) {
		final String url = "/task/"+tkiid+"/variables?deserializeValues=false";
        final JSONObject resp = getRESTObject( identityService.getIdentity(token), url );
		return JSONUtils.convertFromCamunda(resp);
	}

	@Override
	public String claimTask(String token, BoTracking taskParams) throws UnsupportedEncodingException {
	
		final String url = "/task/"+taskParams.getTkiid()+"/claim";
		JSONObject body = new JSONObject();
		body.put("userId", identityService.getIdentity(token).getUsername() );
		HttpHeaders headers = new HttpHeaders();
		headers.add(HttpHeaders.CONTENT_TYPE, "application/json");
		postREST( identityService.getIdentity(token), url, body.toString(), headers );
	
		return "{}";
	}

	@Override
	public String saveTask(String token, BoTracking taskParams, String params) throws UnsupportedEncodingException {
		dataValidationService.validate(params);

		final String url = "/task/"+taskParams.getTkiid()+"/variables";
		JSONObject body = new JSONObject();
		body.put("modifications", JSONUtils.convertToCamunda(new JSONObject(params)));
		body.put("deletions", new JSONArray());
		HttpHeaders headers = new HttpHeaders();
		headers.add(HttpHeaders.CONTENT_TYPE, "application/json");
		postREST( identityService.getIdentity(token), url, body.toString(), headers );

		return "{}";
	}

	@Override
	public String finishTask(String token, BoTracking taskParams, String params) throws UnsupportedEncodingException {
		dataValidationService.validate(params);

		final String url = "/task/"+taskParams.getTkiid()+"/complete";
		JSONObject body = new JSONObject();
		body.put("variables", JSONUtils.convertToCamunda(new JSONObject(params)));
		HttpHeaders headers = new HttpHeaders();
		headers.add(HttpHeaders.CONTENT_TYPE, "application/json");
		postREST( identityService.getIdentity(token), url, body.toString(), headers );
		return "{}";
	}

    @Override
	public String getXml(String token, String processDefKey) {
		JSONObject response = getRESTObject( identityService.getIdentity(token), "/process-definition/key/" + processDefKey + "/xml");
        if (response != null) {
        	return response.optString("bpmn20Xml");
        }
        return null;
    }

    @Override
    public List<Statistic> getStatistics(String token, String key) {
    	List<Statistic> statistics = new ArrayList<>();
        JSONArray statisticsArr = getRESTArray( identityService.getIdentity(token), "/process-definition/key/" + key + "/statistics");
        if (statisticsArr != null) {
        	statisticsArr.forEach(statistic -> {
            	JSONObject statisticObj = (JSONObject) statistic;
                statistics.add(new Statistic(statisticObj.optString("id"), statisticObj.getInt("instances")));
			});
        }
        return statistics;
    }

	@Override
	protected String getBaseUrl() {
		return baseUrl;
	}

}
