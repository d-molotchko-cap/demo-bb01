package com.capbpm.application.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import com.capbpm.application.model.security.Identity;
import com.capbpm.application.model.security.Token;
import com.capbpm.application.service.IdentityService;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Maps;

@Service
class IdentityServiceImpl extends RestService implements IdentityService {
	static final private HttpHeaders headers = new HttpHeaders();
	static {
		headers.add("Content-Type", "application/json");
	}
	
	static final private BiMap<Identity, Token> tokens = Maps.synchronizedBiMap( HashBiMap.<Identity, Token>create() );  

    @Value("${proxy.url}")
    private String baseUrl;

	@Override
	public String login(Identity identity) {
		if( isValidUser(identity) ) {
			return createOrGetToken(identity).getToken();
		}
		return null;
	}

	@Override
	public void logout(String token) {
		synchronized (tokens) {
			tokens.inverse().remove(new Token(token));
		}
	}
    
	@Override
	public boolean isValidUser(Identity identity) {
		final JSONObject user = new JSONObject();
		user.put("username", identity.getUsername());
		user.put("password", identity.getPassword());
		final String url = "/identity/verify";
		try {
			final JSONObject resp = new JSONObject(postREST( identity, url, user.toString(), headers ));
			return resp.optBoolean("authenticated", false);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return false;
	}

	public String getBaseUrl() {
		return baseUrl;
	}

	private Token createOrGetToken(Identity identity) {
		synchronized (tokens) {
			if( tokens.containsKey(identity) ) {
				return tokens.get(identity);
			}
			
			final Token token = new Token(identity);
			tokens.put(identity, token);
			return token;
		}
	}

	@Override
	public Identity getIdentity(String token) {
		Token t = new Token(token);
		if( !tokens.containsValue(t) ) {
			throw new HttpClientErrorException(HttpStatus.UNAUTHORIZED);
		}
		return tokens.inverse().get(t);
	}
	
	@Override
	public List<String> groups(Identity identity) {
		final String url = "/identity/groups?userId="+identity.getUsername();
		try {
			final JSONObject resp = getRESTObject( identity, url );
			JSONArray groups = resp.optJSONArray("groups");
			List<String> rc = new ArrayList<>();
			for( int i=0;  groups != null  &&  i<groups.length();  ++i ) {
				JSONObject group = groups.optJSONObject(i);
				if( group != null ) {
					rc.add(group.getString("id"));
				}
			}
			return rc;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return Collections.emptyList();
	}
	
}

