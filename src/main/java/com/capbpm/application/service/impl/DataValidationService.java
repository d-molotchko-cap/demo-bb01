package com.capbpm.application.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.ValidationException;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import org.everit.json.schema.Schema;
import org.everit.json.schema.Validator;
import org.everit.json.schema.event.CombinedSchemaMismatchEvent;
import org.everit.json.schema.event.ConditionalSchemaMismatchEvent;
import org.everit.json.schema.event.ValidationListener;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONTokener;

@Service
public class DataValidationService {
	static class JSONValidationErrorCollector implements ValidationListener {
		private List<String> messages = new ArrayList<>();
		
		public String getDetails() {
			return messages.stream().collect(Collectors.joining("|"));
		}

		public boolean hasErrors() {
			return messages.size() > 0;
		}
		
		@Override
		public void combinedSchemaMismatch(CombinedSchemaMismatchEvent event) {
			String title = event.getSchema().getTitle();
			if (title != null) {
				title += ": "+event.getFailure().getErrorMessage();
			}
			
			if( title == null ) {
				title = event.getFailure().getErrorMessage();
			}
			
			if( title != null ) {
				messages.add( title);
			}
		}

		@Override
		public void ifSchemaMismatch(ConditionalSchemaMismatchEvent event) {
			String title = event.getSchema().getTitle();
			if (title != null) {
				title += ": "+event.getFailure().getErrorMessage();
			}
			
			if( title == null ) {
				title = event.getFailure().getErrorMessage();
			}
			
			if( title != null ) {
				messages.add( title);
			}
		}

		@Override
		public void thenSchemaMismatch(ConditionalSchemaMismatchEvent event) {
			String title = event.getSchema().getTitle();
			if (title != null) {
				title += ": "+event.getFailure().getErrorMessage();
			}
			
			if( title == null ) {
				title = event.getFailure().getErrorMessage();
			}
			
			if( title != null ) {
				messages.add( title);
			}
		}

		@Override
		public void elseSchemaMismatch(ConditionalSchemaMismatchEvent event) {
			String title = event.getSchema().getTitle();
			if (title != null) {
				title += ": "+event.getFailure().getErrorMessage();
			}
			
			if( title == null ) {
				title = event.getFailure().getErrorMessage();
			}
			
			if( title != null ) {
				messages.add( title);
			}
		}
	};
	
	@Autowired
	private ResourceLoader resourceLoader;	
	
	public void validate(String params) throws ValidationException {
		if( params != null ) {
			JSONObject vars = new JSONObject(params);
			JSONValidationErrorCollector errorCollector = new JSONValidationErrorCollector();
			String[] names = JSONObject.getNames(vars);
			if( names != null ) {
				for (String name : names ) {
					try (InputStream inputStream = resourceLoader.getResource("classpath:validation/" + name + ".json")
							.getInputStream()) {
						JSONObject rawSchema = new JSONObject(new JSONTokener(inputStream));
						Schema schema = SchemaLoader.load(rawSchema);
						
						Validator validator = Validator.builder().withListener( errorCollector ).build();
						validator.performValidation(schema, vars.get(name));
					} catch (IOException e) {
					} catch (org.everit.json.schema.ValidationException e) {
					}
				}
			}
			if( errorCollector.hasErrors() ) {
				throw new ValidationException(errorCollector.getDetails());
			}
		}
	}
}
