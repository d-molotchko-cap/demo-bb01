package com.capbpm.application.service.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

@Service
@PropertySource("classpath:/jdbc.properties")
public class JDBCExecutor {

	@Value("${dashboard.jdbc.url:haha}")
	private String url;

	@Value("${dashboard.jdbc.username:kuku}")
	private String username;

	@Value("${dashboard.jdbc.password:kaka}")
	private String password;
	
	public Map<String, Object> execute(String sql) {
		Map<String, Object> rc = new HashMap<>();
		try {
			Connection c = null;
			Statement statement = null;
			ResultSet resultSet = null;
			try {
				c = getConnection();
				statement = c.createStatement();
				resultSet = statement.executeQuery(sql);

				ResultSetMetaData metaData = resultSet.getMetaData();
				while (resultSet.next()) {
					for (int index = 1; index <= metaData.getColumnCount(); index++) {
						rc.put(metaData.getColumnName(index).toUpperCase(), resultSet.getObject(index));
					}
					break;
				}

			} finally {
				if (resultSet != null) {
					resultSet.close();
				}

				if (statement != null) {
					statement.close();
				}
				if (c != null) {
					c.close();
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return rc;
	}


	private Connection getConnection() throws SQLException {
		return DriverManager.getConnection(url, username, password);
	}
	
}
