package com.capbpm.application.service;

import java.util.List;

import com.capbpm.application.model.security.Identity;

public interface IdentityService {
	String login(Identity identity);
	void logout(String token);
	boolean isValidUser(Identity identity);
	Identity getIdentity(String token);
	List<String> groups(Identity identity);
}
