package com.capbpm.application.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.capbpm.application.model.BoTracking;
import com.capbpm.application.model.ProcessInstance;
import com.capbpm.application.model.Statistic;
import com.capbpm.application.service.WorkflowService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/workflow")
public class BPMController extends SecuredController {

@Value("${acronyms}")
private String[] acronyms;


@Autowired
private WorkflowService bpmService;

	@CrossOrigin()
	@GetMapping("/api/v1/instances")
	ResponseEntity<List<ProcessInstance>> getInstances(
	    @RequestHeader("Authorization")String authorization)
    {
    	return ResponseEntity.ok(bpmService.getInstances(extractToken(authorization), acronyms));
    }

	@CrossOrigin()
    @PostMapping("/api/v1/instance")
    ResponseEntity<String> launch(@RequestHeader("Authorization")String authorization,
        @RequestParam("acronym")String acronym,
        @RequestBody(required=false) String body )
    {
        return ResponseEntity.ok(bpmService.start(extractToken(authorization), acronym, body));
    }

	@CrossOrigin()
    @GetMapping("/api/v1/task/{tkiid}")
    ResponseEntity<String> getTaskData(
        @RequestHeader("Authorization")String authorization,
        @PathVariable("tkiid")String tkiid)
    {
        return ResponseEntity.ok(bpmService.getTaskData(extractToken(authorization), tkiid));
    }

	@CrossOrigin()
    @PutMapping("/api/v1/task/{tkiid}/{piid}")
    ResponseEntity<String> saveTask(
        @RequestHeader("Authorization")String authorization,
        @PathVariable("tkiid")String tkiid,
        @PathVariable("piid")String piid,
        @RequestParam("params")String params,
        @RequestBody String data) throws UnsupportedEncodingException
    {
		BoTracking boTracking = null;
		try {
			boTracking = new ObjectMapper().readValue(params, BoTracking.class);
	        boTracking.setPiid(piid);
	        boTracking.setTkiid(tkiid);
		} catch (IOException e) {
			e.printStackTrace();
		}
        return ResponseEntity.ok(bpmService.saveTask(extractToken(authorization), boTracking, data));
    }

	@CrossOrigin()
    @PostMapping("/api/v1/task/{tkiid}/{piid}/claim")
    ResponseEntity<String> claimTask(
        @RequestHeader("Authorization")String authorization,
        @PathVariable("tkiid")String tkiid,
        @PathVariable("piid")String piid,
        @RequestParam("params")String params) throws UnsupportedEncodingException
    {
		BoTracking boTracking = null;
		try {
			boTracking = new ObjectMapper().readValue(params, BoTracking.class);
	        boTracking.setPiid(piid);
	        boTracking.setTkiid(tkiid);
		} catch (IOException e) {
			e.printStackTrace();
		}
        return ResponseEntity.ok(bpmService.claimTask(extractToken(authorization), boTracking));
    }

	@CrossOrigin()
    @PostMapping("/api/v1/task/{tkiid}/{piid}")
    ResponseEntity<String> finishTask(
        @RequestHeader("Authorization")String authorization,
        @PathVariable("tkiid")String tkiid,
        @PathVariable("piid")String piid,
        @RequestParam("params")String params,
        @RequestBody String data) throws UnsupportedEncodingException
    {
		BoTracking boTracking = null;
		try {
			boTracking = new ObjectMapper().readValue(params, BoTracking.class);
	        boTracking.setPiid(piid);
	        boTracking.setTkiid(tkiid);
		} catch (IOException e) {
			e.printStackTrace();
		}
        return ResponseEntity.ok(bpmService.finishTask(extractToken(authorization), boTracking, data));
    }

	@CrossOrigin()
    @GetMapping("api/v1/diagram/{key}")
    ResponseEntity<String> getDiagramXml(
        @RequestHeader("Authorization")String authorization,
        @PathVariable String key)
    {
        return ResponseEntity.ok(bpmService.getXml(extractToken(authorization), key));
    }

	@CrossOrigin()
    @GetMapping("api/v1/statistics/{key}")
    ResponseEntity<List<Statistic>> getProcessStatistics(
        @RequestHeader("Authorization")String authorization,
        @PathVariable String key)
    {
        return ResponseEntity.ok(bpmService.getStatistics(extractToken(authorization), key));
    }
}
