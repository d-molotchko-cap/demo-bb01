package com.capbpm.application;

import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;
import org.springframework.web.cors.CorsConfiguration;
import static java.util.Arrays.asList;

@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors()
                    .configurationSource(request -> {
                        CorsConfiguration corsConfiguration = new CorsConfiguration();
                        corsConfiguration.setAllowedOrigins(asList(CorsConfiguration.ALL));
                        corsConfiguration.setAllowedHeaders(asList(CorsConfiguration.ALL));
                        corsConfiguration.setAllowedMethods(asList(CorsConfiguration.ALL));
                        corsConfiguration.setAllowCredentials(true);
                        return corsConfiguration;
                    })
                    .and()
                .authorizeRequests()
                    .anyRequest().permitAll()
                    .and()
                .csrf()
	                .disable()
	            .logout()
	            	.logoutSuccessHandler(new HttpStatusReturningLogoutSuccessHandler());
    }
}
