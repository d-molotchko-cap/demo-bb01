package com.capbpm.application.model.notification;

public class Button {

    private String text;
    private String classes;

    public Button() {
    }

    public Button(String text, String classes) {
        this.text = text;
        this.classes = classes;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getClasses() {
        return classes;
    }

    public void setClasses(String classes) {
        this.classes = classes;
    }

}
