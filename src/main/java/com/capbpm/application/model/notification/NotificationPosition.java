package com.capbpm.application.model.notification;

public enum NotificationPosition {
    topRight,
    topCenter,
    topLeft,
    topFullWidth,
    bottomRight,
    bottomCenter,
    bottomLeft,
    bottomFullWidth
}
