package com.capbpm.application.model.notification;

public class NotificationMessage extends Message {

    public NotificationMessage() {
    }

    public NotificationMessage(Message message) {
        this.setMessage(message.getMessage());
        this.setPayload(message.getPayload());
    }

    public NotificationMessage(Message message, NotificationPosition notificationPosition, NotificationType notificationType) {
        this.setMessage(message.getMessage());
        this.setPayload(message.getPayload());
        this.position = notificationPosition;
        this.type = notificationType;
    }

    private NotificationPosition position;
    private NotificationType type;

    public NotificationPosition getPosition() {
        return position;
    }

    public void setPosition(NotificationPosition position) {
        this.position = position;
    }

    public NotificationType getType() {
        return type;
    }

    public void setType(NotificationType type) {
        this.type = type;
    }
}
