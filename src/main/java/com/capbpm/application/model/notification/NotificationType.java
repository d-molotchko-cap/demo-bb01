package com.capbpm.application.model.notification;

public enum  NotificationType {
    success,
    error,
    warning,
    info
}
