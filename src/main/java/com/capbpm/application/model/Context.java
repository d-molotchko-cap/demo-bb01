package com.capbpm.application.model;

public class Context {

    private final String baseUrl;

    public Context(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getBaseUrl() {
        return baseUrl;
    }
}
