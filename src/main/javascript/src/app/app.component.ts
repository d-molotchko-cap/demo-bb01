import { Component, OnInit, OnDestroy } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NotificationService } from './services/notification.service';
import { LoginService } from './services/login.service';
import {AppConfigService} from "./services/app-config.service";
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  // Branding & Company info
  public backgroundColor;
  public companyName: String = 'Kuku';
  public companyWWW: String = '';
  public companyAddress: String = '';
  public companyEmail: String = '';
  public companyPhone: String = '';
  public companyFax: String = '';
  public principal: string;
  protected groups: string[];
  private principalSub;
  private groupsSub;
  public pages = [
  ];

  constructor(
    private translate: TranslateService,
    private notification: NotificationService,
    private loginService: LoginService,
    private sanitizer: DomSanitizer,
    appConfigService: AppConfigService,
    public  router: Router
  ) {
    this.backgroundColor = this.sanitizer.bypassSecurityTrustStyle('background-color: #ffe599 !important');
    appConfigService.runAfterInit((appConfig) => notification.initializeWebSocketConnection(appConfig.endpoint));
    translate.addLangs(['ar', 'de', 'en', 'es', 'fr', 'it', 'ja', 'ko', 'pt']);
    translate.setDefaultLang('en');

    const browserLang = translate.getBrowserLang();
    translate.use(browserLang.match(/ar|de|en|es|fr|it|ja|ko|pt/) ? browserLang : 'en');
  }

  ngOnInit() {
    this.principalSub = this.loginService.principal.subscribe(data => {
        this.principal = data;
    });
    this.groupsSub = this.loginService.groups.subscribe(groups => {
        this.groups = groups;
    });
  }

  ngOnDestroy(): void {
    this.principalSub.unsibscribe();
    this.groupsSub.unsibscribe();
  }

  onLogout(event: Event) {
    event.preventDefault();
    event.stopPropagation();

    this.loginService.logout();

  }
}
