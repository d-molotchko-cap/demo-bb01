import { NgModule } from '@angular/core';

/*Bootstrap*/
import { BsDatepickerModule, ModalModule, BsDropdownModule, TabsModule, CollapseModule } from 'ngx-bootstrap';
import { DataTablesModule } from 'angular-datatables';

@NgModule({
  imports: [BsDatepickerModule.forRoot(), ModalModule.forRoot(), DataTablesModule, BsDropdownModule.forRoot(),
	  		TabsModule.forRoot(), CollapseModule.forRoot()],
  exports: [BsDatepickerModule, ModalModule, DataTablesModule, BsDropdownModule, TabsModule, CollapseModule],
})
export class AppUIModule {}
