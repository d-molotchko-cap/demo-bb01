import { AppConfigService } from './../../../services/app-config.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '../../../services/http-client.service';
import { Observable } from 'rxjs';
import {Statistic} from "../../../model/statistic";

@Injectable({
	providedIn: 'root'
})
export class DiagramService {

  constructor(private http: HttpClient,
		      private appConfigService: AppConfigService) {
  }

  getDiagram(key: string): Observable<any> {
      const options = {
          responseType: 'text' as 'text'
      };
      return this.http.get(this.appConfigService.getUrl(`workflow/api/v1/diagram/${key}`), <any>options);
  }

  getStatistics(key: string): Observable<Statistic[]> {
    return this.http.get<Statistic[]>(this.appConfigService.getUrl(`workflow/api/v1/statistics/${key}`));
  }
}
