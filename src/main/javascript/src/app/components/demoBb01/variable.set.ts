import { Person } from '../../model/person';

export interface VariableSet {
  person: Person;
}

export const uiVariables = [];
