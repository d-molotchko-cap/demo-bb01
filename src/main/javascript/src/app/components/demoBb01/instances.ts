import {VariableSet} from './variable.set';

export class Instances {
    static data: VariableSet = {'person': {
    'firstName': '',
    'lastName': '',
    'address': {
        'city': '',
        'id': '',
        'line2': '',
        'line1': ''
    },
    'id': ''
}};
}
