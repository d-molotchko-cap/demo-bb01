import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription ,  interval } from 'rxjs';
import { isNil, isPlainObject, isEmpty, cloneDeep, isEqual  } from 'lodash';
import { NgRedux } from '@angular-redux/store';
import { FORM_STORE_KEY } from '../../reducer';
import { BPMService, DataTransformer } from '../../services/bpm.service';
import { FileStorageService } from '../../services/file-storage.service';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { ToastrManager} from 'ng6-toastr-notifications';

// Data types
import { VariableSet, uiVariables } from './variable.set';

// Modals
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';


// Services
import { Instances } from './instances';
import { LoginService } from '../../services/login.service';
import { NotificationService } from '../../services/notification.service';
import { ObjectFieldAccessor } from '../../services/objectFieldAccessor.service';
import { ConfirmationModalService } from '../../services/confirmation-modal.service';
import { AppConfigService } from '../../services/app-config.service';
import { ExcelImporter } from '../../services/import/excel-importer.service';

const boTracking = {
  layout: {
    uuid: '',
    title: ''
  },
  milestone: {
    uuid: '',
    title: ''
  },
  node: {
    uuid: '',
    title: ''
  }
};

@Component({
    templateUrl: './activity2.component.html',
    styleUrls: ['./activity2.component.css']
})
export class Activity2Component implements OnInit, OnDestroy {
    private subData;
    public data: VariableSet;
    public loader;
    private tkiid;
    private piid;
    private userId;
    private userGroups;
    private subForm;
    private modalRef: BsModalRef;
    private calculated = [];
    private notificationEndpoints: string[] = [];
    private subscriptions: Subscription[] = [];

	private _ngForm: NgForm;
    @ViewChild('activity2Form', {static: false}) set ngForm(form: NgForm) {
    	this._ngForm = form;
        if (form) {
            let _data = null;
            this.subForm = form.valueChanges.subscribe(data => {
                if (form.dirty && !isEqual(this.data, _data)) {
                    this.calculated.forEach(calcFn => calcFn(this.data));
                    _data = cloneDeep(this.data);
                    this.ngRedux.dispatch({ type: 'CREATE_INSTANCE', store: 'demoBb01', data: this.data});
                }
            });
        }
     }

    constructor (
        private ngRedux: NgRedux<any>,
        private modalService: BsModalService,
        private service: BPMService,
        private route: ActivatedRoute,
        private router: Router,
        private notificationService: NotificationService,
        private fileStorageService: FileStorageService,
        private dataTransformer: DataTransformer,
        private objectFieldAccessor: ObjectFieldAccessor,
        private location: Location,
        private confirmationModalService: ConfirmationModalService,
        private appConfigService: AppConfigService,
        private toastrManager: ToastrManager,
        private excelImporter: ExcelImporter,
        private loginService: LoginService
    ) {
        appConfigService.runAfterInit((appConfig) => {
            const that = this;
            setTimeout(() => {
			    that.notificationEndpoints.forEach(notificationEndpoint => {
				    const subscription = this.notificationService.subscribeToastr(notificationEndpoint);
				    if (subscription) {
					    that.subscriptions.push(subscription.subscribe(notificationMessage => {
						    that.notificationService.renderToastr(notificationMessage)
					    }));
				    }
			    });
	        }, 2000);
        });
    }

    ngOnInit() {
        this.tkiid = this.route.snapshot.paramMap.get('id');
        this.piid = this.route.snapshot.paramMap.get('piid');
		this.userId = this.loginService.user.name;
		this.userGroups = this.loginService.user.groups;
		this.service.claimTask(this.tkiid, this.piid, boTracking ).subscribe(data=>{});
        this.subData = this.ngRedux.select<any>(FORM_STORE_KEY).subscribe(
            data => {
                let _data = data['demoBb01'];
//                if (!isEqual(this.data, _data)) {
                    this.data =  this.applyData(_data);
//                }

            }
        );


        this.ngRedux.dispatch({type: 'LOAD_TASK_DATA', empty: Instances.data, tkiid: this.tkiid, store: 'demoBb01'});

    }

    ngOnDestroy() {
		if( this.subData ){
	        this.subData.unsubscribe();
	    }
		if( this.subForm ){
        	this.subForm.unsubscribe();
       	}
        this.subscriptions.forEach(subscription => subscription.unsubscribe());
    }


	onBack() {
	  this.back();
	}

    onSave() {
        this.ngRedux.dispatch({
            type: 'SAVE_TASK',
            data: this.getData(this.data),
            params: { tkiid: this.tkiid, piid: this.piid, boTracking: boTracking,
                succCallback: () => this.toastrManager['successToastr'](
                    'Saved!'
                ),
                errCallback: (e) => this.toastrManager['errorToastr'](
                    e.error.message.replace(/\|/g, '<br/>'),
                    e.error.error,
                    {enableHTML: true}
                )
            }
        });
    }

    onSubmit() {

        // loader on
        this.ngRedux.dispatch({
            type: 'FINISH_TASK',
            data: this.getData(this.data),
            params: { tkiid: this.tkiid, piid: this.piid, boTracking: boTracking, 
            	succCallback: () => this.router.navigate(['/']),
            	errCallback: (e) => this.toastrManager['errorToastr'](
                    e.error.message.replace(/\|/g, '<br/>'),
                    e.error.error,
                    {enableHTML: true}
                )
            }
        });

    }

    applyValue( varname: string, value: any ) {
        this.objectFieldAccessor.setValue( this.data, varname, value );
    }

    // Handlers
    handleFileInput( event, listeners ) {
        const v: File = event.target.files.item(0);

        const reader = new FileReader();
        reader.onload = function (evt) {
            const file = {
                name: v.name,
                type: v.type,
                content: btoa(evt.target.result)
            };

            this.applyValue(event.target.dataset.varname, file);

            listeners.forEach(handler => {
                handler.bind(this)(event);
            });

            this.fileStorageService.upload(this.piid, file).subscribe(function(response){
                this.applyValue(event.target.dataset.varname + '.uuid', response);
            }.bind(this));
        }.bind(this);
        reader.readAsBinaryString(v);
    }


    back() {
        this.location.back();
    }

    importIntoTable(table: DatatableComponent, field: string) {
        event.preventDefault();
        event.stopPropagation();

        // make a call to import service, provide columns
        // tslint:disable-next-line:max-line-length
        const cols = table.bodyComponent.columns.filter(c => c.prop !== 'kuku123kaka').map(c => { return {'prop': c.prop, 'title': c.name}; });

        const subscr = this.excelImporter.import( cols ).subscribe( data => {
        	const parts = field.split('.');
        	let ref = this.data; 
        	for( let i=0;  i<parts.length-1;  ++i ) {
        		if( !ref[parts[i]] ) {
        			ref[parts[i]] = {};
        		}
				ref = ref[parts[i]];
        	}
            ref[parts[parts.length-1]] = data;
            table.rows = [...table.rows];
        },
        error => {},
        () => {
            subscr.unsubscribe();
        });
    }
    
    private getData(data: VariableSet) {
    	const copy = Object.assign({}, data);
    	uiVariables.forEach( varName => delete copy[varName] );
    	return copy;
    }
    
    private applyData(_data): VariableSet {
    	const copy = Object.assign( this.data||{}, _data );
    	return copy;
    }
    
    private removeItem(arr, item){
        const index = arr.indexOf(item);
        if (index !== -1) {
            arr.splice(index, 1);
        }
    }
}
