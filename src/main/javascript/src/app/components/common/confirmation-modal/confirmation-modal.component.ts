import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';

@Component({
  selector: 'modal-content',
  templateUrl: './confirmation-modal.component.html',
  styleUrls: ['./confirmation-modal.component.css']
})
export class ConfirmationModalComponent implements OnInit {
    title: string;
    message: string;
    okBtnName: string;
    closeBtnName: string;

    public onClose: Subject<boolean>;

    constructor(public bsModalRef: BsModalRef) {}

    ngOnInit() {
        this.onClose = new Subject();
    }

    onConfirm(): void {
        this.onClose.next(true);
        this.bsModalRef.hide();
    }

    onCancel(): void {
        this.onClose.next(false);
        this.bsModalRef.hide();
    }

}
