import { Pipe, PipeTransform } from '@angular/core';
import { isPlainObject } from 'lodash';
import { isString } from 'util';
import { filter } from 'rxjs/operators';

@Pipe({name: 'filter'})
export class FilterPipe implements PipeTransform {
    transform(values: object[], flt): object[] {
        return values.filter(value => this.contains(value, flt));
    }

    private contains(obj: object, flt): boolean {
        return this.findInAllFields(obj, flt);
    }

    private findInFields(obj: object, flt: any): boolean {
        for (const key in Object.keys(flt)) {
            if (key !== 'text') {
                if (this.findInAllFields(obj, flt[key], key)) {
                    return true;
                }
            }
        }
        return false;
    }

    private findInAllFields(obj: object, text: any, keyf?: string): boolean {
        if (text === undefined || text === '') {
            return true;
        }
        text = text.toLowerCase();
        const keys = Object.keys(obj);
        for (const keyIndex in keys) {
            const key = keys[keyIndex];
            if ((keyf && keyf === key) || (!keyf) ) {
                if (isPlainObject(obj[key])) {
                    if (this.contains(obj[key], text)) {
                        return true;
                    }
                } else {
                    if (('' + obj[key]).toLowerCase().includes(text)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
