import { Pipe, PipeTransform } from '@angular/core';
import { isPlainObject } from 'lodash';
import { isString } from 'util';
import { filter } from 'rxjs/operators';

@Pipe({name: 'secured'})
export class SecuredPipe implements PipeTransform {
    transform(values: object[], flt): object[] {
        return values.filter(page => page["groups"].filter(x => flt.includes(x)).length);
    }
}
