import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';

@Component({
    selector: 'column-filter',
    templateUrl: './column-filter.component.html',
    styleUrls: ['./column-filter.component.css']
})
export class ColumnFilterComponent implements OnInit {

    @Input()
    column: any;

    @Input()
    filter: any;

    @Input()
    field?: any;

    @Input()
    table: DatatableComponent;

    @Output()
    updateFilter: EventEmitter < any > = new EventEmitter();

    showControls = false;
    $sort = 0; // -1, 0, 1
    flt: any;

    public operators = [{
            id: 'equals',
            label: '==',
            label2: 'Equals'
        },
        {
            id: 'not_equals',
            label: '<>',
            label2: 'Not Equals'
        },
        {
            id: 'startsWith',
            label: 'abc*',
            label2: 'Starts with'
        },
        {
            id: 'endsWith',
            label: '*abc',
            label2: 'Ends with'
        },
        {
            id: 'contains',
            label: '*abc*',
            label2: 'Contains'
        },
        {
            id: 'any',
            label: '*',
            label2: 'Any'
        }
    ];

    constructor() {}

    ngOnInit() {
        const prop = this.field || this.column.prop;
        if (!this.filter.fields[prop]) {
            this.filter.fields[prop] = {
                operator: 'any',
                value: ''
            };
        }
        this.flt = this.filter.fields[prop];

        this.table.sort.subscribe(data => {
            if (data.sorts  &&  data.sorts[0]) {
                if (data.sorts[0].prop !== (this.field || this.column.prop)) {
                    this.$sort = 0;
                }
            }
        });
    }

    sort() {
        if (this.$sort === 0) {
            this.$sort = 1;
        } else if (this.$sort === 1) {
            this.$sort = -1;
        } else {
            this.$sort = 0;
        }

        let sorts = [];
        if (this.$sort !== 0) {
            sorts = [ {prop: this.field || this.column.prop, dir: this.$sort === 1 ? 'asc' : 'desc'} ];
        }
        this.table.onColumnSort({
            sorts: sorts
        });
    }

    operatorLabel(id) {
        const labels = this.operators.filter(op => op.id === id).map(op => op.label);
        if (labels.length > 0) {
            return labels[0];
        }
        return id;
    }

    setOperator(id) {
        this.flt.operator = id;
        this.emit();
    }

    emit(value ? ) {
        if (value !== undefined) {
            this.flt.value = value;
        }
        this.updateFilter.emit(Object.assign({}, this.filter));
    }

}