import { Pipe, PipeTransform } from '@angular/core';
import { isPlainObject } from 'lodash';
import { isString } from 'util';
import { filter } from 'rxjs/operators';

export interface WindowPipeOptions {
  start: number,
  size: number
};

@Pipe({name: 'window'})
export class WindowPipe implements PipeTransform {
    transform(values: object[], options: WindowPipeOptions): object[] {
        return values.slice( options.start, options.start+options.size);
    }
}
