import { Pipe, PipeTransform } from '@angular/core';
import { isPlainObject } from 'lodash';
import { isString } from 'util';
import { filter } from 'rxjs/operators';
import { ObjectFieldAccessor } from '../../services/objectFieldAccessor.service';

@Pipe({name: 'columnFilter'})
export class ColumnFilterPipe implements PipeTransform {

    constructor(
        private ofa: ObjectFieldAccessor
    ){
    }

    transform(values: object[], flt): object[] {
        return values.filter(value => this.leave(value, flt));
    }

    private leave(obj: object, flt): boolean {
        if( flt.fields ){
            var fields = Object.keys(flt.fields);
            for( var i=0;  i<fields.length;  ++i ){
                const comp = flt.fields[fields[i]];
                if( !comp.value ){
                    continue;
                }
                if( !this[comp.operator]( this.ofa.getValue(obj, fields[i]), comp.value ) ){
                    return false;
                }
            }
        }

        if( flt.global ){
//            return this.findInFields( obj, flt.global );
        }

        return true;
    }

    private any(){
        return true;
    }

    private equals( val1, val2 ){
        return val1 == val2;
    }

    private startsWith( val1, val2 ){
        return val1.indexOf(val2) == 0;
    }

    private endsWith( val1, val2 ){
        return val1.indexOf(val2) == val1.length - val2.length;
    }

    private not_equals( val1, val2 ){
        return val1 != val2;
    }

    private contains( val1, val2 ){
        return (''+val1).indexOf(''+val2) != -1;
    }

    private findInFields(obj: object, flt: any): boolean {
        for (const key in Object.keys(flt)) {
            if (key !== 'text') {
                if (this.findInAllFields(obj, flt[key], key)) {
                    return true;
                }
            }
        }
        return false;
    }

    private findInAllFields(obj: object, text: any, keyf?: string): boolean {
        if (text === undefined || text === '') {
            return true;
        }
        text = text.toLowerCase();
        const keys = Object.keys(obj);
        for (const keyIndex in keys) {
            const key = keys[keyIndex];
            if ((keyf && keyf === key) || (!keyf) ) {
                if (isPlainObject(obj[key])) {
                    if (this.contains(obj[key], text)) {
                        return true;
                    }
                } else {
                    if (('' + obj[key]).toLowerCase().includes(text)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
