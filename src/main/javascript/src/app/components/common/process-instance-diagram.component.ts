import {Component} from '@angular/core';
import {BsModalRef} from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {ProcessInstance} from "../../model/processInstance";

@Component({
    selector: 'modal-content',
    template: `
      <div class="modal-header">
        <h4 class="modal-title pull-left">Process Instance</h4>
        <button type="button" class="close pull-right" aria-label="Close" (click)="bsModalRef.hide()">
            <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div *ngIf="processInstance && processInstance.instance && processInstance.instance.key">
            <app-diagram-viewer [diagramId]="processInstance.instance.key" [nodeId]="processInstance.subject.key"></app-diagram-viewer>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" (click)="bsModalRef.hide()">Close</button>
      </div>
    `
})
export class PIDiagramComponent {
    processInstance: ProcessInstance;
    constructor(public bsModalRef: BsModalRef) {
    }
}
