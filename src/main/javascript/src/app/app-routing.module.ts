import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Step1Component as DemoBb01Step1Component } from './components/demoBb01/step1.component';
import { ActivityComponent as DemoBb01ActivityComponent } from './components/demoBb01/activity.component';
import { Activity2Component as DemoBb01Activity2Component } from './components/demoBb01/activity2.component';

import { DashboardComponent } from './components/common/dashboard.component';
import { AuthGuard } from './services/auth-guard.service';
import { LoginComponent } from './components/common/login.component';
import { LoginService } from './services/login.service';
import { DataResolver } from './services/bpm.service';

export const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        component: DashboardComponent,
        canActivate: [AuthGuard]
    },
    {
        pathMatch: 'full',
        path: 'demoBb01/step1/:id/:piid',
        component: DemoBb01Step1Component,
        canActivate: [AuthGuard],
        resolve: {
          data : DataResolver
        }
    },
    {
        pathMatch: 'full',
        path: 'demoBb01/activity/:id/:piid',
        component: DemoBb01ActivityComponent,
        canActivate: [AuthGuard],
        resolve: {
          data : DataResolver
        }
    },
    {
        pathMatch: 'full',
        path: 'demoBb01/activity2/:id/:piid',
        component: DemoBb01Activity2Component,
        canActivate: [AuthGuard],
        resolve: {
          data : DataResolver
        }
    },
    { path: 'login', component: LoginComponent },
    { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      routes,
      {}
    )
  ],
  providers: [
    AuthGuard,
    LoginService,
    DataResolver
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
