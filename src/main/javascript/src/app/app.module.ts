import { NgModule, APP_INITIALIZER } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { AppRoutingModule } from './app-routing.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { AppUIModule } from './app-ui.module';
import { NgReduxModule, NgRedux } from '@angular-redux/store';
import { createEpicMiddleware } from 'redux-observable';
import { ToastrModule } from 'ng6-toastr-notifications';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Translation
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient as HttpClientAngular } from '@angular/common/http';

// Charts
import { ChartModule } from './chart/chart.module';

// Common components
import { TableComponent } from './components/common/table.component';
import { PIDiagramComponent } from './components/common/process-instance-diagram.component';
import { FileStorageComponent } from './components/common/file-storage.component';
import { FileServiceListComponent } from './components/common/file-service-list.component';
import { FileServiceDownloadComponent } from './components/common/file-service-download.component';
import { ConfirmationModalComponent } from './components/common/confirmation-modal/confirmation-modal.component';

// Validators
import { MinValueValidatorDirective } from './components/validators/min.directive';
import { MaxValueValidatorDirective } from './components/validators/max.directive';
import { PatternValidatorDirective } from './components/validators/pattern.directive';

// Soft validations
import { SoftRequiredDirective } from './components/validators/soft-required.directive';
import { SoftMinDirective } from './components/validators/soft-min.directive';
import { SoftMaxDirective } from './components/validators/soft-max.directive';
import { SoftMinLengthDirective } from './components/validators/soft-minlength.directive';
import { SoftMaxLengthDirective } from './components/validators/soft-maxlength.directive';
import { SoftPatternDirective } from './components/validators/soft-pattern.directive';

import { FilterPipe } from './components/common/filter.pipe';
import { SecuredPipe } from './components/common/secured.pipe';
import { WindowPipe } from './components/common/window.pipe'

// Diagrams
import { BpmnJsModule } from './bpmn-js/bpmn-js.module';

import { AppComponent } from './app.component';
import { rootReducer, INITIAL_STATE } from './reducer';

import { Step1Component as DemoBb01Step1Component } from './components/demoBb01/step1.component';
import { ActivityComponent as DemoBb01ActivityComponent } from './components/demoBb01/activity.component';
import { Activity2Component as DemoBb01Activity2Component } from './components/demoBb01/activity2.component';

import { HttpClient } from './services/http-client.service';
import { LoginService } from './services/login.service';
import { LoginComponent } from './components/common/login.component';
import { DashboardComponent } from './components/common/dashboard.component';
import { BPMService, BPMEpics, DataTransformer } from './services/bpm.service';
import { ObjectFieldAccessor } from './services/objectFieldAccessor.service';

import { NotificationService } from './services/notification.service';
import { FileStorageService } from './services/file-storage.service';
import { FileServiceService } from './services/file-service.service';
import { AppConfigService } from './services/app-config.service';
import { ConfirmationModalService } from './services/confirmation-modal.service';
import { ColumnFilterPipe } from './components/common/column-filter.pipe';
import { ColumnFilterComponent } from './components/common/column-filter/column-filter.component';
import { ExcelImportComponent } from './services/import/excel-import/excel-import.component';



const translateModule = TranslateModule.forRoot({
  loader: {
      provide: TranslateLoader,
      useFactory: HttpLoaderFactory,
      deps: [HttpClientAngular]
  }
});

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClientAngular) {
  return new TranslateHttpLoader(http, 'assets/lang/', '/translations.json');
}

export function initAppConfig(appConfigService: AppConfigService) {
  return () => appConfigService.load();
}

@NgModule({
  imports: [
    BrowserModule, FormsModule, ReactiveFormsModule, AppRoutingModule, HttpClientModule, NgxDatatableModule,
    AppUIModule, translateModule, NgReduxModule, ChartModule, BpmnJsModule, ToastrModule.forRoot(), BrowserAnimationsModule
  ],
  declarations: [ AppComponent
    , SoftRequiredDirective
    , SoftMaxDirective
    , SoftMaxLengthDirective
    , SoftMinDirective
    , SoftMinLengthDirective
    , SoftPatternDirective
    , MinValueValidatorDirective
    , MaxValueValidatorDirective
    , PatternValidatorDirective
    , TableComponent
    , LoginComponent
    , PIDiagramComponent
    , ExcelImportComponent
    , DashboardComponent
    , FilterPipe
    , SecuredPipe
    , ColumnFilterPipe
    , WindowPipe
    , ColumnFilterComponent
    , FileServiceListComponent
    , FileServiceDownloadComponent
    , FileStorageComponent
    , ConfirmationModalComponent
    , DemoBb01Step1Component
    , DemoBb01ActivityComponent
    , DemoBb01Activity2Component
  ],
  providers: [
    BPMService,
    AppConfigService,
    NotificationService,
    { provide: APP_INITIALIZER, useFactory: initAppConfig, deps: [AppConfigService], multi: true },
    BPMEpics,
    DataTransformer,
    FileStorageService,
    FileServiceService,
    ObjectFieldAccessor,
    HttpClient,
    ConfirmationModalService,
    DatePipe
  ],
  entryComponents: [
    PIDiagramComponent,
    ConfirmationModalComponent,
    ExcelImportComponent
  ],
  exports: [ RouterModule ],
  bootstrap: [ AppComponent ]
})
export class AppModule {
  constructor(
      private ngRedux: NgRedux<any>,
      private bpmEpics: BPMEpics
  ) {
    const epicMiddleware = createEpicMiddleware();
    ngRedux.configureStore(rootReducer, INITIAL_STATE, [epicMiddleware]);
    epicMiddleware.run(this.bpmEpics.getInstances);
    epicMiddleware.run(this.bpmEpics.loadTask);
    epicMiddleware.run(this.bpmEpics.finishTask);
    epicMiddleware.run(this.bpmEpics.saveTask);
  }
}
