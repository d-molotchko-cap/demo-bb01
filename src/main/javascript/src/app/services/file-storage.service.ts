import { AppConfigService } from './app-config.service';
import { Injectable } from '@angular/core';
import { HttpClient } from './http-client.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
	providedIn: 'root'
})
export class FileStorageService {

    constructor(
        private http: HttpClient,
        private appConfigService: AppConfigService
    ) {
    }

    upload( piid, file: File ): Observable<String> {
        const self = this;
        return this.http.post(this.appConfigService.getUrl('file-storage/upload/' + piid),
            {
                name: file.name,
                type: file.type,
                content: btoa(file['content'])
            }
        )
            .pipe(map(resp => resp['uuid']));
    }

    list( piid ): Observable<any> {
        const self = this;
        return this.http.get(this.appConfigService.getUrl('file-storage/list/' + piid));
    }

    delete( piid, uuid ): void {
        const self = this;
        this.http.delete(this.appConfigService.getUrl('file-storage/delete/' + piid));
    }
}
