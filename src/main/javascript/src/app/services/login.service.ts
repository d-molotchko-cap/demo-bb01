import { Injectable, Component, EventEmitter } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import {AppConfigService} from "./app-config.service";

@Injectable({
	providedIn: 'root'
})
export class LoginService {
    isLoggedIn = false;
    redirectUrl: string;
    private authToken: string;
    public principal: EventEmitter<string> = new EventEmitter();
    public groups: EventEmitter<string[]> = new EventEmitter();
	public user:{name:string, groups:string[]};

    constructor(
        private http: HttpClient,
        private router: Router,
        private appConfigService: AppConfigService
    ) {
        this.login = this.login.bind(this);
    }

    login(username, password): Observable<boolean> {
        const self = this;
        return this.http.post(this.appConfigService.getUrl('login'), {username: username, password: password})
            .pipe(
                catchError(err => of(false)),
                map(resp => {
                    self.authToken = resp['token'];
                    self.isLoggedIn = !!self.authToken;
                    if (self.isLoggedIn) {
						this.user = {
							name: username,
							groups: resp['groups']
						};
                        self.principal.emit(username);
                        self.groups.emit(resp['groups']);
                    }
                    return self.isLoggedIn;
                }));
    }

    logout() {
        const value: string = 'Bearer ' + this.token();
        this.http.post(this.appConfigService.getUrl('logout'), {}, {
            headers: {
                'Authorization': value
            }
        }).subscribe(
            data => {
                this.isLoggedIn = false;
                delete this.authToken;
				this.user = null;
                this.principal.emit(null);
                this.router.navigate(['/login']);
            },
            (err: HttpErrorResponse) => {
                console.log(err.message);
            }
        );
    }

    token() {
        return this.authToken;
    }
}
