import { Injectable } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Observable } from 'rxjs';
import { ConfirmationModalComponent } from '../components/common/confirmation-modal/confirmation-modal.component';

export interface ModalProperties {
  title: string;
  message: string;
  okBtnName: string;
  closeBtnName: string;
}

@Injectable({
  providedIn: 'root'
})
export class ConfirmationModalService {
    constructor(private modalService: BsModalService) {}

    showModal(initialState: ModalProperties): Observable<boolean> {
        return new Observable(observer => {
            const bsModalRef = this.modalService.show(ConfirmationModalComponent, {initialState});
            bsModalRef.content.onClose.subscribe(result => {
                if (result) {
                    observer.next(true);
                }
                observer.error();
            });
        });
    }
}
