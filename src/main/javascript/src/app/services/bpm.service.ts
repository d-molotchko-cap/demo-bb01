import { AppConfigService } from './app-config.service';
import { Injectable } from '@angular/core';
import { HttpClient } from './http-client.service';
import { ProcessInstance } from '../model/processInstance';
import { Resolve, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { isNil, isPlainObject, isEmpty, cloneDeep } from 'lodash';

import {ActionsObservable, ofType } from 'redux-observable';
import { Observable, of } from 'rxjs';
import { mergeMap, map, catchError, take } from 'rxjs/operators';

const api = {
    process: {
        list: 'workflow/api/v1/instances',
        launch: 'workflow/api/v1/instance?acronym={acronym}'
    },
    task: {
        getData: 'workflow/api/v1/task/{tkiid}',
        finish:  'workflow/api/v1/task/{tkiid}/{piid}?params={params}',
        save:    'workflow/api/v1/task/{tkiid}/{piid}?params={params}',
        claim:   'workflow/api/v1/task/{tkiid}/{piid}/claim?params={params}'
    }
};

@Injectable({
	providedIn: 'root'
})
export class BPMService {

    constructor(
        private http: HttpClient,
        private appConfigService: AppConfigService
    ) {
    }

    getInstances(): Observable<ProcessInstance[]> {
        const url = api.process.list;
        return this.http.get<ProcessInstance[]>( this.appConfigService.getUrl(url) );
    }

    getTaskData(tkiid: string) {
        return this.http.get(this.appConfigService.getUrl(api.task.getData.replace('{tkiid}', tkiid)));
    }

    claimTask(tkiid: string, piid: string, boTracking? ) {
        return this.http.post(
            this.appConfigService.getUrl(
                api.task.claim
                    .replace('{tkiid}', tkiid)
                    .replace('{piid}',  piid )
                    .replace('{params}', encodeURIComponent(JSON.stringify(boTracking ? boTracking : {})))
            ), {} );
    }

    saveTask(tkiid: string, piid: string, data: string, boTracking? ) {
        return this.http.put(
            this.appConfigService.getUrl(
                api.task.save
                    .replace('{tkiid}', tkiid)
                    .replace('{piid}',  piid )
                    .replace('{params}', encodeURIComponent(JSON.stringify(boTracking ? boTracking : {})))
            ), data );
    }

    finishTask(tkiid: string, piid: string, data: string, boTracking? ) {
        return this.http.post(
        		this.appConfigService.getUrl(
	                api.task.finish
	                    .replace('{tkiid}', tkiid)
	                    .replace('{piid}',  piid )
	                    .replace('{params}', encodeURIComponent(JSON.stringify(boTracking ? boTracking : {})))
	            ), data);
    }

    launch(id: string, data?: string) {
        return this.http.post(this.appConfigService.getUrl(api.process.launch.replace('{acronym}', id)), data);
    }
}

@Injectable({
	providedIn: 'root'
})
export class DataResolver implements Resolve<any> {
    constructor(private service: BPMService, private router: Router) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
        const tkiid = route.paramMap.get('id');

        return this.service.getTaskData(tkiid)
            .pipe(
                take(1),
                map(data => {
                    if (data) {
                        return data;
                    } else { // id not found
                        this.router.navigate(['/']);
                        return null;
                    }
                })
            );
    }
}

@Injectable({
	providedIn: 'root'
})
export class DataTransformer {

    public fixEmptyObjects(src, target) {
        const self = this;

        if (isEmpty(target)) {
            Object.keys(src).forEach(function (varname) {
                target[varname] = cloneDeep(src[varname]);
            });
        }

        Object.keys(target).forEach(function (varname) {
            if (target[varname] === undefined || target[varname] == null) {
                Object.keys(src).forEach(function (vn) {
                    target[vn] = cloneDeep(src[vn]);
                });
            } else {
                if (!!src[varname]) {
                    Object.keys(src[varname]).forEach(function (field) {
                        if (isPlainObject(src[varname][field])) {
                            if (isNil(target[varname][field])) {
                                target[varname][field] = {};
                            }
                            self.fixEmptyObjects(src[varname][field], target[varname][field]);
                        }
                    });
                }
            }
        });

        // fix dates
        this.fixDates(src, target);
        return target;
    }

    public fixDates(src, target) {
        const self = this;
        Object.keys(src).forEach(function (field) {
            const obj1 = src[field];
            if (!!target[field]) {
                if (obj1 instanceof Date) {
                    target[field] = new Date(target[field]);
                } else if (isPlainObject(obj1)) {
                    self.fixDates(obj1, target[field]);
                }
            }
        });
    }

    public prepareData(data: any): string {
        this.removeFiles(data);
        return JSON.stringify(data, (k, v) => {
            if (v instanceof Date) {
                return v.toISOString();
            }
            return v;
        });
    }

    public removeFiles(data) {
        Object.keys(data).forEach(function(k){
            if (data[k]  == null  ||  data[k]  === undefined) {
                delete data[k];
            }
            if (!!data[k] &&  !!data[k]['name'] && !!data[k]['type']  &&  !!data[k]['uuid']  &&  !!data[k]['content']) {
                delete data[k];
            }
            if (!!data[k]  &&  isPlainObject(data[k])) {
                this.removeFiles(data[k]);
            }
        }.bind(this));
    }
}

@Injectable({
	providedIn: 'root'
})
export class BPMEpics {
    constructor(private bpmService: BPMService, private dt: DataTransformer ) {
    }

    loadTask = (action$: ActionsObservable<any>): Observable<any> => {
        return action$.pipe(
            ofType('LOAD_TASK_DATA'),
            mergeMap(({tkiid, store, empty}) => {
                return this.bpmService.getTaskData(tkiid)
                    .pipe(
                        map(result => ({type: 'CREATE_INSTANCE', store: store, data: this.dt.fixEmptyObjects(empty, result)})),
                        catchError(error => of({type: 'LOAD_TASK_DATA_FAILED'}))
                    );
            })
        );
    };

    finishTask = (action$: ActionsObservable<any>): Observable<any> => {
        return action$.pipe(
            ofType('FINISH_TASK'),
            mergeMap(({data, params}) => {
                return this.bpmService.finishTask(params.tkiid, params.piid, this.dt.prepareData(data), params.boTracking)
                    .pipe(
                        map(result => ({type: 'FINISH_TASK_SUCCESSED', callback: params.succCallback})),
                        catchError(error => of({type: 'FINISH_TASK_FAILED', error: error, callback: params && params.errCallback}))
                    );
            })
        );
    };

    saveTask = (action$: ActionsObservable<any>): Observable<any> => {
        return action$.pipe(
            ofType('SAVE_TASK'),
            mergeMap(({data, params}) => {
                return this.bpmService.saveTask(params.tkiid, params.piid, this.dt.prepareData(data), params.boTracking)
                    .pipe(
                        map(result => ({type: 'SAVE_TASK_SUCCESSED', callback: params.succCallback})),
                        catchError(error => of({type: 'SAVE_TASK_FAILED', error: error, callback: params && params.errCallback}))
                    );
            })
        );
    };

    getInstances = (action$: ActionsObservable<any>): Observable<any> => {
        return action$
            .pipe(
                ofType('GET_INSTANCES'),
                mergeMap(({data, params}) => {
                    return this.bpmService.getInstances()
                        .pipe(
                            map(result => ({
                                type: 'GET_INSTANCES_SUCCESSED',
                                data: result,
                                callback: params && params.succCallback
                            })),
                            catchError(error => of({
                                type: 'GET_INSTANCES_FAILED',
                                callback: params && params.errCallback
                            }))
                        )
                })
            );
    };

}
