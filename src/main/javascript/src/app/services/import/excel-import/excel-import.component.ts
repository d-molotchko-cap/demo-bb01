import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import * as XLSX from 'xlsx';

@Component({
  selector: 'modal-content',
  templateUrl: './excel-import.component.html',
  styleUrls: ['./excel-import.component.css']
})
export class ExcelImportComponent implements OnInit {

    file: String;
    imported = false;
    loaded = false;
    wb: XLSX.WorkBook;
    sheetNames: string[] = [];
    sheetName: string;
    data: any[] = [];
    convertedData: any[] = [];
    preview: string;
    columns: {prop: string, title: string, mapTo?: string}[] = [];
    columnNames: string[] = [];
    isPreviewCollapsed = true;
    rows = {
        first: 1,
        last: 0
    };

    constructor(
        public bsModalRef: BsModalRef
    ) {}

    ngOnInit() {

    }

    importData() {
        this.convertedData = [];
        for ( let row = this.rows.first;  row <= this.rows.last;  ++row) {
            const item = {};
            for (let col = 0; col < this.columns.length; ++col) {
                const column = this.columns[col];
                if (column.mapTo) {
                    this.setValue( item, column.prop, this.data[row][column.mapTo] );
                }
            }
            this.convertedData.push(item);
        }
        this.imported = true;
    }

    setValue(item: {}, prop: string, value: any) {
        const fields = prop.split('.');
        for (let i = 0;  i < fields.length - 1;  ++i) {
            if ( !item[fields[i]]) {
                item[fields[i]] = {};
            }
            item = item[fields[i]];
        }
        item[fields[fields.length - 1]] = value;
    }

    onSheetNameChange() {
        const ws: XLSX.WorkSheet = this.wb.Sheets[this.sheetName];

        this.data = XLSX.utils.sheet_to_json(ws, {header: 'A', blankrows: false});
        if ( this.data.length > 0 ) {
            const row = this.data[0];
            this.columnNames = Object.keys(row);
            this.rows.first = 1;
            this.rows.last = this.data.length - 1;
            
            let mapped = false;
            
            // check for 
            for(let i=0;  i<this.columns.length;  ++i ){
                const column = this.columns[i];
                column.mapTo = this.columnNames[i%this.columnNames.length];
            }

            // 1-1 mapping
            if( !mapped ){
                for(let i=0;  i<this.columns.length;  ++i ){
                    const column = this.columns[i];
                    
                    for (let j=0;  j<this.columnNames.length;  ++j) {
                        const value = row[this.columnNames[j]];
                        if (column.prop == value) {
                            column.mapTo = this.columnNames[j];
                            mapped = true;
                        }
                    }
                }
            }
        }
        

        this.preview = XLSX.utils.sheet_to_html(ws);
    }

    onFileChange(evt: any) {
        /* wire up file reader */
        const target: DataTransfer = <DataTransfer>(evt.target);
        if (target.files.length !== 1) {
            throw new Error('Cannot use multiple files');
        }
        const reader: FileReader = new FileReader();
        reader.onload = (e: any) => {
          /* read workbook */
          const bstr: string = e.target.result;
          this.wb = XLSX.read(bstr, {type: 'binary'});

          /* grab first sheet */
          this.sheetNames = this.wb.SheetNames;
          this.sheetName = this.sheetNames[0];
          this.loaded = true;
          this.onSheetNameChange();
        };
        reader.readAsBinaryString(target.files[0]);
    }

}
