import { Injectable } from "@angular/core";
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { ExcelImportComponent } from './excel-import/excel-import.component';
import { Observable, Subscription } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ExcelImporter {
    bsModalRef: BsModalRef;
    constructor(
        private modalService: BsModalService
    ) {
    }

    import<T>(cols: {prop: string, title: string}[]): Observable<T[]> {
        //

        return new Observable<T[]>( (observer) => {
            this.bsModalRef = this.modalService.show(ExcelImportComponent, {class: 'modal-lg', initialState: {cols: cols}});
            this.bsModalRef.content.columns = cols;

            const subsciption = this.modalService.onHide.subscribe(param => {
                console.log(param);

                if ( this.bsModalRef.content.imported ) {
                    const data: T[] = this.bsModalRef.content.convertedData;
                    observer.next( data );
                    observer.complete();
                } else {
                    observer.complete();
                }
            });

            return {unsubscribe() {subsciption.unsubscribe(); }};
        });
    }
}
