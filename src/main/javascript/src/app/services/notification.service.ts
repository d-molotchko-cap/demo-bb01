import { Injectable } from '@angular/core';
import uuid from 'uuid';
import Stomp from 'stompjs';
import SockJS from 'sockjs-client';
import { Subject } from 'rxjs';
import {NotificationMessage} from '../model/notification/notification-message';
import {Toastr, ToastrManager} from 'ng6-toastr-notifications';
import {NotificationType} from '../model/notification/notification-type';

@Injectable({
	providedIn: 'root'
})
export class NotificationService {
    private serverUrl = 'socket'; // <endpoint> - socket 
    private stompClient;
    private subscribers = {};
    private id;

    private subjectToastr = {};
    private notificationEndpoints: string[] = [];

    constructor(private toastrManager: ToastrManager) {
        this.id = uuid.v4();
    }

    initializeWebSocketConnection(rootUrl: string = '/') {
        const ws = new SockJS(rootUrl + this.serverUrl);
        this.stompClient = Stomp.over(ws);
        const that = this;
        this.stompClient.connect({}, (frame) => {
            //Notifications
            this.notificationEndpoints.forEach(notificationEndpoint => {
                this.subjectToastr[notificationEndpoint] = new Subject<any>();
                this.stompClient.subscribe(notificationEndpoint,
                    notificationMessage => {
                        this.subjectToastr[notificationEndpoint].next(JSON.parse(notificationMessage.body));
                    }
                )
            })
        });
    }

    notify( datatype, instance ) {
        const msg = {
            clientId: this.id,
            data: instance
        };
        if (this.stompClient) {
        	this.stompClient.send('/app/send/' + datatype, {}, JSON.stringify(msg));    // <app_prefix> - /app
        }
    }

    subscribe( datatype, callback ) {
        let list = this.subscribers[datatype];
        if ( !list ) {
            this.subscribers[datatype] = [];
            list = this.subscribers[datatype];
        }

        list.push(callback);
    }

    private propagateChanges( datatype, data ) {
        const list = this.subscribers[datatype];
        if ( list ) {
            list.forEach( callback => {
                callback(data);
            });
        }
    }

    //Notifications
    private methodNameToastr(notificationType: any) {
        switch (notificationType) {
            case NotificationType[NotificationType.warning]:
                return 'warningToastr';
            case NotificationType[NotificationType.error]:
                return 'errorToastr';
            case NotificationType[NotificationType.success]:
                return 'successToastr';
            default:
                return 'infoToastr'
        }
    }

    subscribeToastr(topic: string): Subject<NotificationMessage> {
        return this.subjectToastr[topic] || null;
    }

    renderToastr(notificationMessage: NotificationMessage): Toastr {
        const options = {
            position: null,
            enableHTML: false
        };
        let html = '';
        if (notificationMessage.position ) {
            options.position = notificationMessage.position.toString().replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
        }
        if (notificationMessage.buttons && notificationMessage.buttons.length) {
            options.enableHTML = true;
            html += '<div>';
            notificationMessage.buttons.forEach(button => {
                html += '<button class="' + button.classes + '">' + button.text + '</button>'
            });
            html += '</div>';
        }

        return this.toastrManager[this.methodNameToastr(notificationMessage.type)](
            html,
            notificationMessage.message,
            options
        );
    }
}