import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IAppConfig } from '../model/app-config';
import { environment } from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class AppConfigService {

    disabled = true;
    inited = false;
    callbacks: any[] = [];

    private appConfig: IAppConfig = {
        endpoint: ''
    };

    constructor(private http: HttpClient) {
        this.inited = true;
        this.appConfig.endpoint = environment.endpoint;
    }

    enable() {
        this.disabled = false;
    }

    private runInitCallbacks() {
        this.callbacks.forEach(callback => callback(this.appConfig));
    }

    load() {
        this.http.get('assets/config/config.json')
        .toPromise()
        .then((appConfig: IAppConfig) => {
            this.inited = true;
            if (appConfig.endpoint  &&  !appConfig.endpoint.startsWith('@')) {
                this.appConfig = appConfig;
            }
            this.runInitCallbacks();
        })
        .catch(err => this.inited = true);
    }

    runAfterInit(callback) {
        if (callback) {
            if (this.inited) {
                callback(this.appConfig);
            } else {
                this.callbacks.push(callback);
            }
        }
    }

    getUrl(endpoint: string) {
    	return endpoint;
    }
}