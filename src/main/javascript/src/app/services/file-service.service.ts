import { AppConfigService } from './app-config.service';
import { Injectable } from '@angular/core';
import { HttpClient } from './http-client.service';
import { Observable } from 'rxjs';

export interface Credential {
    username: String;
    password: String;
    url: String;
}

export interface FileContent {
    base64: String;
}

export interface FileMetadata {
    type?: string;
    key?: string;
    path: string;
    name: string;
    size?: number;
    created?: Date;
    modified?: Date;
    createdBy?: string;
    modifiedBy?: string;
    content?: FileContent;
}

@Injectable({
	providedIn: 'root'
})
export class FileServiceService {

    constructor(
        private http: HttpClient,
        private appConfigService: AppConfigService
    ) {
    }

    list( url: String, credential: Credential, folder: String ): Observable<any> {
        return this.http.post(this.appConfigService.getUrl(url + '/list' + folder), credential);
    }

    download( url: String, file: FileMetadata ): Observable<any> {
        return this.http.post(this.appConfigService.getUrl(url + '/get'), file);
    }

    addFile( url: String, credential: Credential, file: FileMetadata): Observable<any> {
        return this.http.post(this.appConfigService.getUrl(url + '/create'), {credential: credential, file: file});
    }

    updateFile( url: String, file: FileMetadata): Observable<any> {
        return this.http.post(this.appConfigService.getUrl(url + '/update'), file);
    }

    removeFile( url: String, file: FileMetadata ): Observable<any> {
        return this.http.post(this.appConfigService.getUrl(url + '/delete'), file);
    }
}
