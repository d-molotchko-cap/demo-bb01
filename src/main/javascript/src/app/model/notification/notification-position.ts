export enum NotificationPosition {
    topRight,
    topCenter,
    topLeft,
    topFullWidth,
    bottomRight,
    bottomCenter,
    bottomLeft,
    bottomFullWidth
}