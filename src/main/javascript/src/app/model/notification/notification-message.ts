import {NotificationType} from "./notification-type";
import {NotificationPosition} from "./notification-position";
import {Button} from "./button";

export class NotificationMessage {
    message: string;
    payload: string;
    type: NotificationType;
    position: NotificationPosition;
    buttons: Button[];
}