
export interface Address {
    id: string;
    city: string;
    line1: string;
    line2: string;
}
